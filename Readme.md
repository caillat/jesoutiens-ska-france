# Site de l'expression du soutien français au projet SKA.

## Motivation.

Présenter au public concerné, c'est à dire aux mondes de la Recherche et de l'Industrie,
l'opportunité d'exprimer son soutien à la participation de la France dans le projet SKA.

## Principaux éléments du site.

* Une page d'introduction et formulaire de soutien (public)
* Une page récapitulant les signataires à tout instant (public)
* Une page d'administration/modération (admin)

## Lien.

http://artemix.obspm.fr/le-projet-ska-m-interesse

## Aspects techniques.

### Serveur http

NodeJS

### Base de donn?es.

Mongodb

### Configuration.

Certains éléments de configuration se trouvent dans `config/default.json` et `config/production.json`

### [re]Démarrage et arrêt du serveur.

* Démarrer une session sur `romeo.obspm.fr` avec suffisamment de privilèges pour aller sur le répertoire de `caillat`

* Vérifier que le serveur n'est pas déja en train de tourner (cf. Arrêt du serveur, méthode alternative).

* Lancer le serveur :
```bash
cd ~caillat/le-projet-ska-m-interesse
[caillat@romeo le-projet-ska-m-interesse]$ NODE_ENV=production PORT=3030 ./node_modules/.bin/forever -a -l forever.log -o forever.out -e forever.error  start  bin/www 
```

* Relancer le serveur :
```bash
cd ~caillat/le-projet-ska-m-interesse
[caillat@romeo le-projet-ska-m-interesse]$ NODE_ENV=production PORT=3030 ./node_modules/.bin/forever -a -l forever.log -o forever.out -e forever.error  restart  bin/www 
```

* Arrêter le serveur :
```bash
cd ~caillat/le-projet-ska-m-interesse
[caillat@romeo le-projet-ska-m-interesse]$ NODE_ENV=production PORT=3030 ./node_modules/.bin/forever -a -l forever.log -o forever.out -e forever.error  stop  bin/www 
```

### Arrêt du serveur, méthode alternative.

* Déterminer le numéro de processus du serveur :
```bash
[caillat@romeo ~]$ ps aux|grep bin/www
caillat   16523  0.0  0.0 975692 24340 ?        Ssl  juin02   0:01 /usr/bin/node /home/caillat/le-projet-ska-m-interesse/node_modules/forever/bin/monitor bin/www
caillat   18241  0.0  0.0 112676   972 pts/0    R+   11:41   0:00 grep --color=auto bin/www
caillat   90455  0.0  0.0 1283384 51184 ?       Sl   10:08   0:03 /usr/bin/node /home/caillat/le-projet-ska-m-interesse/bin/www
```

* Arrêter le processus du serveur :
```bash
[caillat@romeo ~]$  kill -9 16253 90455
```
