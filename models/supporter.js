var mongoose = require('mongoose');

module.exports = mongoose.model('Supporter',{
    id: String,
    email: String,
    firstName: String,
    lastName: String,
    institute: String,
    title: String,
    fonction: String,
    pnas : [String],
    comment: String,
    creationDate: Date,
    status: String,
    active: Boolean,
});
