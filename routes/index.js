'use strict';

var express = require('express');
var router = express.Router();
var Supporter = require('../models/supporter');
var _ = require('lodash');
var path = require('path');
var fs = require('fs');


const config = require('config');
let adminEmail = config.adminEmail;
let skafEmail = config.skafEmail;

const nodemailer = require('nodemailer');

let smtpConfig = {
  host: 'smtp.obspm.fr',
  port: 587,
  secure : false,
  auth : {
    user: 'caillat@obspm.fr',
    pass: 'nyc2002'
  },
  logger:true
};


var message;
var action;
var pageTitle = 'Expression d\'int&acute;r&ecirc;t en France pour le projet SKA';

function json2LaTeX() {
  Supporter.find({}, function(err, supporters) {
    let result ="\\documentclass[8pt]{article}\n";
    result += "\\usepackage[margin=0in]{geometry}\n";
    result += "\\usepackage[T1]{fontenc}\n";
    result += "\\usepackage[utf8]{inputenc}\n";
    result += "\\usepackage[francais]{babel}\n";
    result += "\\begin{document}\n";
    result += "\\begin {tabular}{|c|c|c|c|c|c|c|}\n";
    for (var i = 0; i < supporters.length; i++) {
      result += supporters[i].firstName.replace("&", "\\&") + " & "
              + supporters[i].lastName.replace("&", "\\&")+ " & "
              + supporters[i].institute.replace("&", "\\&") + " & "
              + supporters[i].title.replace("&", "\\&") + " & "
              + supporters[i].fonction.replace("&", "\\&") + " & "
              + supporters[i].pnas + " & "
              + supporters[i].comment.replace("&", "\\&") + "\\\\ \n";
    }
    result += "\\end{tabular}\n";
    result += "\\end{document}\n"
    fs.writeFile(path.join(__dirname + '/../public/latex/supporters.tex'), result, function (err) {
      if (err) {
        return console.error(err);
      }
      console.log("List of supporters successfully written");
      return;
    });
  });
}

function sendMailtoSupporter(supporter) {
  let transporter = nodemailer.createTransport(smtpConfig);
  let html = `Bonjour ${supporter.firstName},<br/><br/>

  La coordination SKA-France vous remercie pour votre contribution &agrave; l'expression d'int&eacute;r&ecirc;t pour le projet SKA en France <br/>
  envoy&eacute;e au cours de votre visite sur le site http://artemix.obspm.fr/le-projet-ska-m-interesse <br/><br/>

  Si toutefois il vous apparait que cette contribution a &eacute;t&eacute; &eacute;mise &agrave; votre insu
  <b>signalez-le nous imm&eacute;diatement en cliquant sur le lien suivant http://artemix/le-projet-ska-m-interesse/abus?id=${supporter._id}</b>.<br/><br/>

  Cordialement,
  <br/><br/>
  La coordination SKA-France.
  <hr/>`;

  transporter.sendMail({
    from: adminEmail,
    to: supporter.email,
    subject: "SKA-France -- Votre soutien",
    html:html
  });

  transporter.close();
};

function sendMailtoManager(supporter) {
  let transporter = nodemailer.createTransport(smtpConfig);
  let html = `Bonjour,<br/><br/>

  Une nouveau t&eacute;moignage d'int&eacute;r&ecirc;t pour le projet SKA en France a &eacute;t&eacute; envoy&eacute; par ${supporter.email}.
  <br/><hr/>`;

  transporter.sendMail({
    from: adminEmail,
    to: adminEmail,
    subject : "SKA-France -- Expression d'inter&ecirc;t",
    html : html
  })
}

function options(pnas) {
  // The list of acronyms for Programmes nationaux et actions sp?cifiques.
  const pnas_ = ["----------", "PCMI", "PNCG", "PNGRAM", "PNHE", "PNP", "PNPS", "PNST", "ASA", "ASHRA", "ASOV", "JMMC", "ASSKA LOFAR"];

  var result = "";
  for (var i = 0; i < pnas_.length; i++) {
    result = result + "\n" + "<option ";

    if (pnas && pnas.indexOf(pnas_[i]) !== -1) {
      result = result + " selected";
    }
    result = result + ">" + pnas_[i] + " </option>";
  }
  return result;
};

/* GET home page. */
router.get('/', function(req, res, next) {
  console.log("req.body.message = " + req.body.message);
  if (req.body.message) {
    message = req.body.message;
  }
  else {
    message = '';
  }

  var pnas = [];
  if (req.body.pnas) pnas = req.body.pnas;

  res.render('index', { title: pageTitle, message: message, status: 'closed', action: 'fill', content:{}, options:options(pnas) });
});

/**
* A utility to capitalize all the words of a string.
*/
String.prototype.capitalize = function(allWords) {
  return (allWords) ? // if all words
  this.split(' ').map(word => word.capitalize()).join(' ') : //break down phrase to words then  recursive calls until capitalizing all words
  this.charAt(0).toUpperCase() + this.slice(1); // if allWords is undefined , capitalize only the first word , mean the first char of the whole string
}

/**
*  Handle a post to visualize a signature
*/

router.post('/sign', function (req, res) {

  console.log('index received a post method call');
  var firstName = _.capitalize(req.body.firstName);
  var lastName = _.capitalize(req.body.lastName);
  var email = req.body.email.toLowerCase();
  var institute = req.body.institute;
  var title = req.body.title;
  var fonction = req.body.fonction;
  var pnas = req.body.pnas;
  var comment = req.body.comment;

  var content = {firstName, lastName, institute, title, fonction, email, pnas, comment};
  console.log(content);

  if ((firstName == "")
  || (lastName=="")
  || (institute=="")
  || (title=="")
  || (email=="")) {
    res.render('index',
    {title: title,
      action:'fill',
      message:"Remplissez tous le champs obligatoires (*)",
      content:content,
      options:options(pnas)
    }
  );
  return;
}

action = req.body.action;
console.log("action = " + action);
//
// Output the user input for verification.
if (action=="verify") {
  console.log("About to send this " + content);
  res.render('index', { title: pageTitle,
    action:'previs',
    message: '',
    content:content,
    options:options(pnas)
  });
  return;
}

//
// Record the submission.
if (action=="send") {
  //var message;

  console.log("Looking for email : " + email);
  // Look for email in the database.
  Supporter.findOne({email: email}, function (err, supporter) {
    // Internal error while accessing the database.
    if (err) {
      message = "Erreur &agrave; l'enregistrement : " +err;
      res.render("index", {message:message, title:message, content:content});
      return console.error(message);
    }
    // A signature with the provided email already exists.
    if (supporter) {
      message = "Une contribution avec cette adresse electronique  ('"+email+"') a deja ete envoyee.";
      console.log(message);
      res.render("index", {message:message, title:message, action:'fill', content:content, options:options(pnas)});
      return;
    }
    // Create a new signature and save it in the database.
    else {
      var newSupporter = new Supporter();

      newSupporter.firstName    = firstName;
      newSupporter.lastName     = lastName;
      newSupporter.email        = email;
      newSupporter.pnas         = pnas;
      newSupporter.institute    = institute;
      newSupporter.title        = title;
      newSupporter.fonction     = fonction;
      newSupporter.comment      = comment;
      newSupporter.creationDate = Date.now();
      newSupporter.active       = true;

      console.log("About to create a new supporter " + newSupporter);

      newSupporter.save(function(err) {
        if (err) {
          message = 'Erreur &agrave; l\'enregistrement d\'une nouvelle contribution: ' + err;
          res.render("index", {message:message, title:message, content:content, options:options(pnas)});
          return console.log(message);
        }

        console.log('New signature successfully recorded');

        /* Let's acknowledge the submission */
        sendMailtoSupporter(newSupporter);

        /* Let's the manager know about the submission */
        sendMailtoManager(newSupporter);

        /* Update the LaTeX table */
        //json2LaTeX();

        res.redirect("supporters");
        return;
      });
    }
  });
  return;
}

});

router.get('/supporters', function(req, res) {
  Supporter.find({}, function(err, supporters) {
    res.render("supporters", {supporters : supporters});
  });
});

router.get('/json2latex', function(req, res) {
    json2LaTeX();
    res.redirect("supporters");
});


router.get('/histogram', function(req, res) {
  Supporter.find({}, function(err, supporters) {
    var histogram = {};
    var nsupporters = supporters.length;
    for (var i = 0; i < nsupporters; i++) {
      var domain = supporters[i].email.split('@')[1];
      if (domain in histogram) {
        histogram[domain]+=1;
      }
      else {
        histogram[domain]=1;
      }
    }
    res.render("histogram", {histogram : histogram});
  });
});



/**
* Handle a get userAdmin -> displays the users administration table/form.
*/
var basicAuth = require('basic-auth-connect')('skaf', 'skaffein');
router.get('/admin', basicAuth, function(req, res){
  if ( req.query.delete ) {
    let email2delete = req.query.delete;
    console.log("About to delete " + req.query.delete);
    Supporter.findOne({email: email2delete})
    .exec(function (err, doc) {
      if (err) {
        message = 'Error in signature deletion : ' +err;
        return console.error(message);
      }
      if (doc) doc.remove();
      Supporter.find({}, function(err, supporters) {
        res.render('admin', {user: req.user, title: 'Users administration', supporters:supporters})
        return;
      });
    });
  }
  else {
    Supporter.find({}, function(err, supporters) {
      res.render('admin', {user: req.user, title: 'Users administration', supporters:supporters})
      return;
    });
  }
});

router.get('/skalb-monitor', function(req, res) {
   res.sendFile(path.join(__dirname + '/skalb-monitor.html'));
   return;
});

module.exports = router;
